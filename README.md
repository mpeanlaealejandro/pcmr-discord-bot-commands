# Contributing
If you're handy with coding, simply fork this repo, make your changes, and submit a pull request!  Once it's approved the changes will be available.

If you are not handy with coding, simply open an Issue and someone else may take it on to code.

# Example command
    [
      ...
       "CONTRIBUTE": {
          "name": "How can I contribute to PCMRBot?",
          "text": "PCMRBot as a whole is closed source.  However, these commands are open source.\n\nhttps://gitlab.com/pcmasterrace/discord/pcmr-discord-bot-commands",
          "image": "https://gitlab.com/pcmasterrace/discord/pcmr-discord-bot-commands/-/raw/master/command_images/contribution.png",
          "aliases": [],
          "attribs": []
        },
      ...
    ]
   
`"CONTRIBUTE"` is the command you use in chat.  `?contribute` ( case-insensitive, but always caps here )    
`"name"` is what is shown in the `?help` command list.  `contribute - How can I contribute to PCMRBot?`    
`"text"` is the content of the message.    
`"image"` is a URL for an image to show.  Links to images not in this repository (command_images/) will be rejected.    
`"desc"` is unused and will probably go away.    
`"aliases"` is a list of optional additional commands.    
`"attribs"` is a list of optional attributes.
 - `"noembed"` to make this command not reply in an embed
 - `"hidden"` don't show this command in `?help`

The above will render like:

![contribute_example.png](./contribute_example.png)
